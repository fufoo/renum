package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"go/ast"
	"go/format"
	"go/parser"
	"go/token"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/agnivade/levenshtein"
)

var verbose = 1

// comprehensive mode means we have complete visibility across the project (i.e.,
// the message catalog).  This allows us to determine when messages have been removed
// from the catalog; otherwise, we can only remove messages from the catalog for files
// that we have scanned
var comprehensive = false

var catalogEntry = regexp.MustCompile(`^([A-Z]{3}-[0-9]{4}) ([a-zA-Z0-9/.-]+):([0-9]+)(: (".*"))
`)

func main() {

	v := flag.Bool("verbose", false, "show more of the parse tree")
	q := flag.Bool("quiet", false, "only show problems")
	fix := flag.Bool("fix", false, "fix linting issues")
	writeback := flag.Bool("write", false, "write back fixed file")
	cat := flag.Bool("catalog", false, "dump catalog")

	flag.Parse()

	if *v {
		verbose = 2
	}
	if *q {
		verbose = 0
	}

	fset := token.NewFileSet()

	var pkg []*ast.Package
	var cfg []*config
	nfiles := 0

	var moddir string

	for _, arg := range flag.Args() {
		recursive := false
		if strings.HasSuffix(arg, "/...") {
			arg = strings.TrimSuffix(arg, "/...")
			recursive = true
		}

		mod := findClosestGoModule(arg)
		if moddir == "" {
			moddir = mod
		} else if moddir != mod {
			fmt.Fprintf(os.Stderr, "cannot operate over multiple Go modules!\n")
			fmt.Fprintf(os.Stderr, "   found: %s\n", moddir)
			fmt.Fprintf(os.Stderr, "     and: %s (from %s)\n", mod, arg)
			os.Exit(1)
		}
		for _, arg := range expandRecursion(recursive, arg) {
			c := accumulateConfig(arg, mod)

			pkgs, err := parser.ParseDir(fset, arg, pathFilter, parser.ParseComments)
			if err != nil {
				fmt.Fprintf(os.Stderr, "FAILED: %s\n", err)
				os.Exit(1)
			}
			for name, p := range pkgs {
				nfiles += len(p.Files)
				if verbose >= 2 {
					fmt.Printf("%s - %d files\n", name, len(p.Files))
				}
				pkg = append(pkg, p)
				cfg = append(cfg, c)
			}
		}
	}

	if verbose >= 2 {
		fmt.Printf("walking %d files over %d packages\n", nfiles, len(pkg))
	}

	proj := &project{
		fset: fset,
		requireMessageID: map[Level]bool{
			ERROR:         true,
			AUDIT:         true,
			WARNING:       true,
			INFO:          true,
			WRAPPED_ERROR: true,
			NEW_ERROR:     true,
		},
		catalog:   loadCatalog(filepath.Join(moddir, "message.cat")),
		moduledir: moddir,
	}

	for j, p := range pkg {
		ast.Walk(&packageScope{p.Name, proj, cfg[j]}, p)
	}

	// process the (combined) message catalog
	proj.reconcileCatalog()

	fail := false

	if proj.lintMissing > 0 {
		fmt.Fprintf(os.Stderr, "%d missing message ids\n", proj.lintMissing)
		fail = true
	}
	if proj.lintDuplicate > 0 {
		fmt.Fprintf(os.Stderr, "%d duplicate message ids\n", proj.lintDuplicate)
		fail = true
	}
	if proj.lintNotAllowed > 0 {
		fmt.Fprintf(os.Stderr, "%d message ids not allowed\n", proj.lintNotAllowed)
		fail = true
	}
	if proj.lintDisallowed > 0 {
		fmt.Fprintf(os.Stderr, "%d message ids disallowed\n", proj.lintDisallowed)
		fail = true
	}
	if proj.lintWrapFollowText > 0 {
		fmt.Fprintf(os.Stderr, "%d uses of Wrap() contain follow text\n", proj.lintWrapFollowText)
		fail = true
	}
	if proj.lintMissingText > 0 {
		fmt.Fprintf(os.Stderr, "%d messages are missing descriptive text\n", proj.lintMissingText)
		fail = true
	}

	if *cat {
		var lst []string
		for k := range proj.catalog.index {
			lst = append(lst, k)
		}
		sort.Strings(lst)
		for _, k := range lst {
			for i, occur := range proj.catalog.index[k] {
				if i == 0 {
					fmt.Printf("%s", k)
				} else {
					fmt.Printf("%*s", len(k), "")
				}
				fmt.Printf("\t%s:%d", occur.file, occur.line)
				if occur.text != "" {
					fmt.Printf(": %q", occur.text)
				}
				fmt.Printf("\n")
			}
		}
	}

	if !fail {
		return
	}
	if !*fix {
		os.Exit(1)
	}

	// apply the fixes
	fixfiles := make(map[*fileScope][]LintFix)

	for _, fix := range proj.fixes {
		position := fix.Target()
		fmt.Printf("patch %s:%d\n", position.Filename, position.Line)
		f := fix.File()
		fixfiles[f] = append(fixfiles[f], fix)
	}

	fmt.Fprintf(os.Stderr, "applying fixes to %d files\n", len(fixfiles))
	for fixfile, lst := range fixfiles {
		err := fixfile.fix(lst, *writeback)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err)
			os.Exit(1)
		}
	}
}

func expandRecursion(recur bool, arg string) []string {
	if !recur {
		return []string{arg}
	}

	lst := []string{arg}
	filepath.Walk(arg, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			lst = append(lst, path)
		}
		return nil
	})
	return lst
}

type levelConfig int

const (
	levelOptional = levelConfig(iota)
	levelRequired
	levelProhibited
)

// an entry is metadata that describes treatment for a file;
// the config is a set of entries.  A config line looks like
// either:
//
//     <glob>:ignore
//
// or:
//
//     <glob>:[-]MSG-PATT,...
//
// the former form is used to ignore entirely a given set of files;
// renumbering will not inspect or modify them.
//
// the latter form is used to specify which message id patterns are
// allowed to be used in the file, or, with '-', not allowed to be
// used in the file.
//
// message id patterns may contain 'X' characters in the numeric
// portion, indicating a wildcard.
//
// for example:
//
//     *:FOO-1XXX,FOO-2XXX,trace=never,audit=always
//     *_test.go:TST-XXXX,-FOO-XXXX
//
// specifies that all files in the directory can only use FOO-1XXX
// or FOO-2XXX message ids except the test files which can
// use TST message ids, but *not* FOO message ids
type entry struct {
	pattern  string
	ignore   bool
	allow    []MessageID
	disallow []MessageID
	bylevel  map[Level]levelConfig
}

type config struct {
	entries []entry
}

var configLine = regexp.MustCompile(`^\s*([^#][^:]*):([^#]*)(#.*)?$`)

//
var levelConfigPat = regexp.MustCompile(`^([a-z]+)=(always|never|optional)$`)

// include incorporates a particular config file into a config; when
// scanning a directory, accumulateConfig() walks from the directory
// to the root of the module, incorporating message config files
func (c *config) include(src, top string) {
	rel, err := filepath.Rel(top, filepath.Dir(src))
	if err != nil {
		panic(err)
	}
	if verbose >= 2 {
		fmt.Printf("  including %s [%s]\n", src, rel)
	}
	buf, err := ioutil.ReadFile(src)
	if err != nil {
		panic(err) // could stat it but not read it; maybe missing perms?
	}
	for _, line := range bytes.Split(buf, []byte{'\n'}) {
		m := configLine.FindSubmatch(line)
		if m != nil {
			trim := bytes.TrimSpace(m[1])
			parts := bytes.Split(m[2], []byte{','})
			ent, ok := parseConfigEntry(src, rel, string(trim), parts)
			if ok {
				c.entries = append(c.entries, ent)
			}
		}
	}
}

func (proj *project) relfile(file string) string {
	file, err := filepath.Abs(file)
	if err != nil {
		panic(err)
	}
	file, err = filepath.Rel(proj.moduledir, file)
	if err != nil {
		panic(err)
	}
	return file
}

func (c *config) match(proj *project, file string) entry {
	ent := entry{
		bylevel: make(map[Level]levelConfig),
	}
	for k, v := range proj.requireMessageID {
		if v {
			ent.bylevel[k] = levelRequired
		}
	}

	file = proj.relfile(file)

	if verbose >= 2 {
		fmt.Printf("%q - %d entries in config\n", file, len(c.entries))
	}

	for _, e := range c.entries {
		if verbose >= 2 {
			fmt.Printf("    pattern=%q file=%q\n", e.pattern, file)
		}
		match := globmatcher(e.pattern).MatchString(file)
		//match, _ := filepath.Match(e.pattern, file)
		if match {
			if e.ignore {
				if verbose >= 1 {
					fmt.Printf("%q matches %q : IGNORING\n", file, e.pattern)
				}
				return entry{ignore: true}
			}
			if verbose >= 1 {
				fmt.Printf("%q matches %q : %d allow, %d disallow\n",
					file,
					e.pattern,
					len(e.allow),
					len(e.disallow))
			}
			ent.allow = append(ent.allow, e.allow...)
			ent.disallow = append(ent.disallow, e.disallow...)
			for k, v := range e.bylevel {
				ent.bylevel[k] = v
			}
		}
	}
	return ent
}

// accumulateConfig walks the directory tree, incorporating the
// entries from .renumconfig files into the overall config that should
// be used for a specific directory
func accumulateConfig(dir, top string) *config {
	cfg := &config{}

	top, err := filepath.Abs(top)
	if err != nil {
		panic(err)
	}

	dir, err = filepath.Abs(dir)
	if err != nil {
		panic(err)
	}

	for {
		f := filepath.Join(dir, ".renumconfig")
		sb, err := os.Stat(f)
		if err == nil && (sb.Mode()&(os.ModeType&^os.ModeSymlink)) == 0 {
			cfg.include(f, top)
		}
		if dir == top {
			break
		}
		dir = filepath.Dir(dir)
	}
	return cfg
}

func findClosestGoModule(dir string) string {

	here, err := filepath.Abs(dir)
	if err != nil {
		panic(err)
	}

	for {
		_, err := os.Stat(filepath.Join(here, "go.mod"))
		if err == nil {
			return here
		}
		if here == "/" {
			return "."
		}
		here = filepath.Join(here, "..")
	}
}

type LintFix interface {
	Target() token.Position
	File() *fileScope
	Apply() error
}

type project struct {
	moduledir          string
	fset               *token.FileSet
	requireMessageID   map[Level]bool
	lintMissing        int // required but not supplied
	lintDuplicate      int // used elsewhere
	lintNotAllowed     int // not in the allow list
	lintDisallowed     int // in the disallow list
	lintWrapFollowText int // Wrap() contains follow text, like errlib.Wrap("FOO-1234 this is text")
	lintMissingText    int // message contains no descriptive text, eg log.Errorf(ctx, "FOO-1234")
	catalog            *MessageCatalog
	fixes              []LintFix
	suggestions        map[MessageID]occurrence
}

// this is the hook for when we see a string that has a message id
func (in *fileScope) noticeMessageID(mid MessageID, str, rest string, lit *ast.BasicLit, l *Level) {
	pkg := in.pkg
	p := pkg.project
	pos := p.fset.Position(lit.ValuePos)
	if verbose >= 1 {
		if l == nil {
			fmt.Printf("%s:%d:    [%s-%s] %q\n",
				pos.Filename,
				pos.Line,
				mid.Section,
				mid.Number,
				rest,
			)
		} else {
			fmt.Printf("%s:%d:    %s [%s-%s] %q\n",
				pos.Filename,
				pos.Line,
				*l,
				mid.Section,
				mid.Number,
				rest,
			)
		}
	}
	// make sure the message part is syntactically valid
	if l != nil {
		if *l == WRAPPED_ERROR {
			if rest != "" {
				fmt.Fprintf(os.Stderr, "%s:%d:  Wrap() contains unexpected follow-on text\n",
					pos.Filename,
					pos.Line)
				p.lintWrapFollowText++
			}
		} else {
			if rest == "" {
				fmt.Fprintf(os.Stderr, "%s:%d: %q has no descriptive text\n",
					pos.Filename,
					pos.Line,
					str)
				p.lintMissingText++
			}
		}
	}

	// make sure it is allowed and not disallowed

	var pre string
	if l != nil {
		pre = l.String() + " "
	}

	accept := true // whether we are going to put it in the catalog

	if len(in.cfg.allow) > 0 {
		allowed := false
		for _, a := range in.cfg.allow {
			if mxmatch(mid, a) {
				allowed = true
				break
			}
		}
		if !allowed {
			fmt.Fprintf(os.Stderr, "%s:%d:  %s[%s-%s] is not allowed by config ; should be %q\n",
				pos.Filename,
				pos.Line,
				pre,
				mid.Section,
				mid.Number,
				in.cfg.allow)
			p.lintNotAllowed++
			p.fixes = append(p.fixes,
				&reassignID{
					assignID: assignID{
						in:   in,
						lit:  lit,
						cfg:  in.cfg,
						text: rest,
					},
					old: mid,
				})
			accept = false
		}
	}

	disallowed := false
	for _, d := range in.cfg.disallow {
		if mxmatch(mid, d) {
			disallowed = true
			break
		}
	}
	if disallowed {
		fmt.Fprintf(os.Stderr, "%s:%d:  %s[%s-%s] is disallowed by config\n",
			pos.Filename,
			pos.Line,
			pre,
			mid.Section,
			mid.Number)
		p.lintDisallowed++
		if accept {
			p.fixes = append(p.fixes,
				&reassignID{
					assignID: assignID{
						in:   in,
						lit:  lit,
						cfg:  in.cfg,
						text: rest,
					},
					old: mid,
				})
		}
		accept = false
	}

	// if is it allowed / not disallowed by config, accept it into the catalog
	// (where we might find duplicates)
	if accept {
		occ := occurrence{
			file:   p.relfile(pos.Filename),
			line:   pos.Line,
			column: pos.Column,
			mid:    mid,
			text:   rest,
			origin: originSource,
			reassigner: &reassignID{
				assignID: assignID{
					in:   in,
					lit:  lit,
					cfg:  in.cfg,
					text: rest,
				},
				old: mid,
			},
		}
		k := mid.String()
		p.catalog.index[k] = append(p.catalog.index[k], occ)
	}
}

// This is the hook for when we see a log message WITHOUT a message id
func (in *fileScope) noticeLogger(l Level, str string, lit *ast.BasicLit) {
	p := in.pkg.project
	pos := p.fset.Position(lit.ValuePos)

	if in.cfg.bylevel[l] == levelRequired {
		fmt.Fprintf(os.Stderr, "%s:%d: %s message has no id: %q\n",
			pos.Filename,
			pos.Line,
			l,
			str)
		p.lintMissing++
		p.fixes = append(p.fixes,
			&assignID{
				in:   in,
				lit:  lit,
				text: str,
				cfg:  in.cfg,
			})
	} else if verbose >= 1 {
		fmt.Printf("%s:%d:    %s [no message id] %q\n",
			pos.Filename,
			pos.Line,
			l,
			str)
	}
}

type packageScope struct {
	name    string
	project *project
	config  *config
}

func (p *packageScope) Visit(n ast.Node) ast.Visitor {
	//fmt.Printf("PKG SCOPE %T\n", n)
	if _, ok := n.(*ast.Package); ok {
		return p
	}
	if f, ok := n.(*ast.File); ok {
		pos := p.project.fset.Position(f.Package)
		if verbose >= 2 {
			fmt.Printf("---\n")
			fmt.Printf("%s\n", pos.Filename)
		}
		ent := p.config.match(p.project, pos.Filename)
		if ent.ignore {
			if verbose >= 1 {
				fmt.Printf("skipping %s due to config\n", pos.Filename)
			}
			return nil
		}
		if verbose >= 1 {
			fmt.Printf("allow: %q  disallow: %q\n", ent.allow, ent.disallow)
		}
		return &fileScope{
			pkg:  p,
			self: f,
			cfg:  ent,
		}
	}
	return nil
}

type fileScope struct {
	pkg  *packageScope
	self *ast.File
	cfg  entry
}

func (f *fileScope) filename() string {
	pos := f.pkg.project.fset.Position(f.self.Pos())
	return pos.Filename
}

func (f *fileScope) Visit(n ast.Node) ast.Visitor {
	if fn, ok := n.(*ast.FuncDecl); ok {
		if verbose >= 2 {
			pos := f.pkg.project.fset.Position(fn.Name.NamePos)
			fmt.Printf("%s:%d:  func %s()\n",
				pos.Filename,
				pos.Line,
				fn.Name.Name)
		}
		return &funcScope{
			file: f,
			self: fn,
		}
	}
	return nil
}

type funcScope struct {
	self *ast.FuncDecl
	file *fileScope
	last *ast.CommentGroup
}

func (f *funcScope) Visit(n ast.Node) ast.Visitor {
	if n == nil {
		//fmt.Printf("\\\\\\\\ END FUNC\n")
		return nil
	}
	//fmt.Printf("   -- a %T\n", n)
	return infunc{f, 0}
}

type infunc struct {
	in    *funcScope
	depth int
}

func (in infunc) Visit(n ast.Node) ast.Visitor {
	if n == nil {
		return nil
	}

	//fmt.Printf("[%d] a %T\n", in.depth, n)
	if lit, ok := n.(*ast.BasicLit); ok {
		if lit.Kind == token.STRING {
			proj := in.in.file.pkg.project
			pos := proj.fset.Position(lit.ValuePos)
			str, _ := strconv.Unquote(lit.Value)
			if verbose >= 2 {
				fmt.Printf("%s:%d:%d    STRING %q\n",
					pos.Filename,
					pos.Line,
					pos.Column,
					str)
			}
			mid, rest, ok := parseMessageID(str)
			if ok {
				in.in.file.noticeMessageID(mid, str, rest, lit, nil)
			}
		}
	}

	if call, ok := n.(*ast.CallExpr); ok {
		if sel, ok := call.Fun.(*ast.SelectorExpr); ok {
			if id, ok := sel.X.(*ast.Ident); ok {
				//fmt.Printf("--- %s.%s\n", id.Name, sel.Sel.Name)
				key := id.Name + "." + sel.Sel.Name
				if level, ok := logSelectors[key]; ok {
					//fmt.Printf("call of [%d] %q selector!\n", level, sel.Sel.Name)
					return inlevel{in.in, in.depth + 1, level}
				} else {
					switch sel.Sel.Name {
					case "Tracef",
						"Debugf", "Infof", "Warningf", "Auditf", "Errorf", "Wrap",
						"Newf":
						if verbose >= 1 {
							proj := in.in.file.pkg.project
							pos := proj.fset.Position(sel.Sel.NamePos)
							fmt.Printf("%s:%d: use of %s() not classifiable\n",
								pos.Filename,
								pos.Line,
								sel.Sel.Name)
						}
					}
				}
			}
		}
	}

	return infunc{
		in.in,
		in.depth + 1,
	}
}

type inlevel struct {
	in    *funcScope
	depth int
	l     Level
}

func (in inlevel) Visit(n ast.Node) ast.Visitor {
	if lit, ok := n.(*ast.BasicLit); ok {
		if lit.Kind == token.STRING {
			proj := in.in.file.pkg.project
			pos := proj.fset.Position(lit.ValuePos)
			str, _ := strconv.Unquote(lit.Value)
			if verbose >= 2 {
				fmt.Printf("%s:%d:%d    STRING %q [%d]\n",
					pos.Filename,
					pos.Line,
					pos.Column,
					str,
					in.l)
			}
			mid, rest, ok := parseMessageID(str)
			if ok {
				in.in.file.noticeMessageID(mid, str, rest, lit, &in.l)
			} else {
				in.in.file.noticeLogger(in.l, str, lit)
			}
		}
	}
	if _, ok := n.(*ast.CallExpr); ok {
		return infunc{in.in, in.depth + 1}
	}

	return in
}

type Level int

const (
	TRACE = Level(1 + iota)
	DEBUG
	INFO
	WARNING
	AUDIT
	ERROR
	WRAPPED_ERROR
	NEW_ERROR
)

var levelNames = map[string]Level{
	"TRACE":   TRACE,
	"DEBUG":   DEBUG,
	"INFO":    INFO,
	"WARNING": WARNING,
	"AUDIT":   AUDIT,
	"ERROR":   ERROR,
}

func (l Level) String() string {
	switch l {
	case TRACE:
		return "TRACE"
	case DEBUG:
		return "DEBUG"
	case INFO:
		return "INFO"
	case WARNING:
		return "WARNING"
	case AUDIT:
		return "AUDIT"
	case ERROR:
		return "ERROR"
	case WRAPPED_ERROR:
		return "ERROR(Wrap)"
	case NEW_ERROR:
		return "ERROR(Newf)"
	default:
		return "-invalid-"
	}
}

// this configures what packages we look for logSelectors in (really, we just
// look for these identifiers; we're not doing a complete analysis, only at the AST level)

var logSelectors = map[string]Level{
	"log.Tracef":   TRACE,
	"log.Debugf":   DEBUG,
	"log.Infof":    INFO,
	"log.Warningf": WARNING,
	"log.Auditf":   AUDIT,
	"log.Errorf":   ERROR,
	"errlib.Wrap":  WRAPPED_ERROR,
	"errlib.Newf":  NEW_ERROR,
}

var MessageIDPattern = regexp.MustCompile(`^([A-Z]{3})-([0-9]{3,5})`)
var MessageIDMeta = regexp.MustCompile(`^([A-Z]{3})-([0-9X]{3,5})`)

type MessageID struct {
	Section string
	Number  string
}

func (mid MessageID) String() string {
	return mid.Section + "-" + mid.Number
}

func mustParseMessageID(str string) MessageID {
	mid, rest, ok := parseMessageID(str)
	if !ok {
		panic(fmt.Sprintf("could not parse message id from %q", str))
	}
	if rest != "" {
		panic(fmt.Sprintf("remainder after message id in %q", str))
	}
	return mid
}

func parseMessageID(str string) (MessageID, string, bool) {
	m := MessageIDPattern.FindStringSubmatch(str)
	if m == nil {
		return MessageID{}, str, false
	}
	mid := MessageID{
		Section: m[1],
		Number:  m[2],
	}
	return mid, strings.TrimLeft(str[len(m[0]):], " "), true
}

func parseMessageIDPat(str string) (MessageID, bool) {
	m := MessageIDMeta.FindStringSubmatch(str)
	if m == nil {
		return MessageID{}, false
	}
	mid := MessageID{
		Section: m[1],
		Number:  m[2],
	}
	return mid, true
}

func mxmatch(concrete, pattern MessageID) bool {
	if concrete.Section != pattern.Section {
		return false
	}
	if len(concrete.Number) != len(pattern.Number) {
		return false
	}
	for i := 0; i < len(pattern.Number); i++ {
		p := pattern.Number[i]
		if p != 'X' {
			if p != concrete.Number[i] {
				return false
			}
		}
	}
	return true
}

type idOrigin int

const (
	originCatalog = idOrigin(iota)
	originSource
	originFixup
)

type occurrence struct {
	file       string
	line       int
	column     int
	mid        MessageID
	text       string
	origin     idOrigin
	reassigner *reassignID
}

type fileNumbers struct {
	index     map[string][]occurrence
	committed map[string][]occurrence
}

type MessageCatalog struct {
	index map[string][]occurrence
}

func loadCatalog(srcfile string) *MessageCatalog {
	cat := make(map[string][]occurrence)

	fd, err := os.Open(srcfile)
	if err != nil {
		return &MessageCatalog{
			index: cat,
		}
	}

	defer fd.Close()

	src := bufio.NewReader(fd)
	for {
		line, err := src.ReadBytes('\n')
		if err != nil {
			break
		}
		m := catalogEntry.FindSubmatch(line)
		if m != nil {
			key := string(m[1])
			file := string(m[2])
			line, _ := strconv.Atoi(string(m[3]))
			text, e := strconv.Unquote(string(m[5]))
			if e != nil {
				panic(e)
			}
			fmt.Printf("LD %s %s:%d: %q\n", key, file, line, text)
			o := occurrence{
				file:   file,
				line:   line,
				text:   text,
				mid:    mustParseMessageID(key),
				origin: originCatalog,
			}
			cat[key] = append(cat[key], o)
		}
	}

	return &MessageCatalog{
		index: cat,
	}
}

func iabs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func (o occurrence) distance(to occurrence) (int, string) {
	td := levenshtein.ComputeDistance(o.text, to.text)

	if o.file != to.file {
		return 200 + td, fmt.Sprintf("different files, text diff=%d", td)
	}
	ld := iabs(o.line - to.line)
	return ld + td, fmt.Sprintf("line diff=%d text diff=%d", ld, td)
}

type reassignID struct {
	assignID
	old MessageID
}

func (a reassignID) Apply() error {
	id, ok := a.alloc(&a.old)
	if !ok {
		return fmt.Errorf("could not find a new id to replace %s", a.old.String())
	}

	posn := a.Target()
	fmt.Printf("%s:%d: renumbering message id %s -> %s %s\n",
		posn.Filename,
		posn.Line,
		a.old.String(),
		id.String(),
		a.text)
	a.lit.Value = strconv.Quote(id.String() + " " + a.text)
	return nil
}

type assignID struct {
	in   *fileScope
	lit  *ast.BasicLit
	cfg  entry
	text string
}

func (mid MessageID) spacefor() (int, int) {
	n := 1
	offset := 0
	for i := 0; i < len(mid.Number); i++ {
		if mid.Number[i] == 'X' {
			if n == 1 {
				n = 9
				offset = 1
			} else {
				n = n * 10
				offset = offset * 10
			}
		}
	}
	return n, offset
}

func (mid MessageID) replace(j int) MessageID {
	var num [6]byte
	rplc := strconv.FormatUint(uint64(j), 10)

	for i := 0; i < len(mid.Number); i++ {
		if mid.Number[i] == 'X' {
			num[i] = rplc[0]
			rplc = rplc[1:]
		} else {
			num[i] = mid.Number[i]
		}
	}
	return MessageID{
		Section: mid.Section,
		Number:  string(num[:len(mid.Number)]),
	}
}

func (a assignID) alloc(old *MessageID) (MessageID, bool) {
	if len(a.cfg.allow) == 0 {
		//fmt.Printf("*** %#v\n", a.cfg)
		//fmt.Printf("*** %#v\n", a.in.self)
		panic(fmt.Sprintf("%s: need to infer something from file, package, or module",
			a.Target().Filename))

	}
	proj := a.in.pkg.project

	attempt := func(allow MessageID) (MessageID, bool) {
		space, offset := allow.spacefor()
		for i := 0; i < space; i++ {
			trial := allow.replace(offset + i)
			if len(proj.catalog.index[trial.String()]) == 0 {

				posn := a.Target()
				proj.catalog.index[trial.String()] = []occurrence{
					{
						file:   proj.relfile(posn.Filename),
						line:   posn.Line,
						column: posn.Column,
						mid:    trial,
						text:   a.text,
						origin: originFixup,
					},
				}

				return trial, true
			}
		}
		return MessageID{}, false
	}

	// first pass, try to find one in the same section
	if old != nil {
		for _, allow := range a.cfg.allow {
			if allow.Section == old.Section {
				id, ok := attempt(allow)
				if ok {
					return id, true
				}
			}
		}
	}

	// second pass, just try the allowed ones in order
	for _, allow := range a.cfg.allow {
		id, ok := attempt(allow)
		if ok {
			return id, true
		}

	}
	return MessageID{}, false
}

func (a assignID) Apply() error {
	id, ok := a.alloc(nil)
	if !ok {
		return fmt.Errorf("could not assign a message id")
	}
	posn := a.Target()

	fmt.Printf("%s:%d: new message id: %s %s\n",
		posn.Filename,
		posn.Line,
		id.String(),
		a.text)
	a.lit.Value = strconv.Quote(id.String() + " " + a.text)
	return nil
}

func (a assignID) Target() token.Position {
	return a.in.pkg.project.fset.Position(a.lit.ValuePos)
}

func (a assignID) File() *fileScope {
	return a.in
}

func (f *fileScope) fix(lst []LintFix, writeback bool) error {
	err := f.fixtmp(lst)
	if err == nil && writeback {
		fmt.Printf("copying %s -> %s\n", "/tmp/output.go", f.filename())
		buf, err := ioutil.ReadFile("/tmp/output.go")
		if err != nil {
			panic(err)
		}
		err = ioutil.WriteFile(f.filename(), buf, 0666)
		if err != nil {
			panic(err)
		}
	}
	return err
}

func (f *fileScope) fixtmp(lst []LintFix) error {

	for _, f := range lst {
		err := f.Apply()
		if err != nil {
			return err
		}
	}

	fd, err := os.Create("/tmp/output.go")
	if err != nil {
		return err
	}
	defer fd.Close()
	dst := bufio.NewWriter(fd)
	defer dst.Flush()

	return format.Node(bufio.NewWriter(dst), f.pkg.project.fset, f.self)
}

func parseConfigEntry(src, rel string, trim string, parts [][]byte) (entry, bool) {
	ref := filepath.Join(rel, trim)

	//fmt.Printf("parseConfigEntry: src=%q rel=%q trim=%q\n", src, rel, trim)
	ent := entry{
		pattern: ref,
		bylevel: make(map[Level]levelConfig),
	}

	for _, p := range parts {
		entry := string(bytes.TrimSpace(p))
		if entry != "" {
			if verbose >= 2 {
				fmt.Printf("     %s : %s\n", ref, p)
			}
			ent.appendConfigEntryPart(src, entry)
		}
	}
	return ent, true
}

func (e *entry) appendConfigEntryPart(src string, entry string) {
	if entry == "ignore" {
		e.ignore = true
		return
	}

	if m := levelConfigPat.FindStringSubmatch(entry); m != nil {
		l, ok := levelNames[strings.ToUpper(m[1])]
		if !ok {
			fmt.Fprintf(os.Stderr, "%s: invalid level name %q in: %s\n",
				src, m[1], entry)
			return
		}
		switch strings.ToLower(m[2]) {
		case "always":
			e.bylevel[l] = levelRequired
		case "never":
			e.bylevel[l] = levelProhibited
		case "optional":
			e.bylevel[l] = levelOptional
		}
		return
	}

	midpat := entry
	prohibit := false

	if midpat[0] == '-' {
		prohibit = true
		midpat = midpat[1:]
	}

	midx, ok := parseMessageIDPat(midpat)
	if !ok {
		fmt.Fprintf(os.Stderr, "%s: invalid message id pattern %q\n",
			src, midpat)
		return
	}

	if prohibit {
		e.disallow = append(e.disallow, midx)
	} else {
		e.allow = append(e.allow, midx)
	}
}

func (proj *project) reconcileCatalog() {
	for key, occurs := range proj.catalog.index {
		if len(occurs) > 1 {
			proj.lintDuplicate++
			proj.reconcileDuplicateID(key, occurs)
		}
	}
}

func (proj *project) reconcileDuplicateID(key string, occurs []occurrence) {
	fromcat := -1
	for i, occur := range occurs {
		fmt.Printf("%s:%d: duplicates %q\n", occur.file, occur.line, key)
		// see if we can use the old data from the catalog to guess which
		// one(s) are newly offending
		if occur.origin == originCatalog {
			fromcat = i
		}
	}

	if fromcat >= 0 {
		// if we are in COMPREHENSIVE mode (i.e., we have
		// visibility across the entire project), then we can
		// reconcile messages across files even when files
		// have been deleted.  Otherwise, in limited scope
		// mode (i.e., we have visbility across only parts of
		// the project), we will only consider messages moved
		// across files that we have scanned.
		for i, occur := range occurs {
			if i != fromcat {
				d, desc := occurs[fromcat].distance(occur)
				fmt.Printf("%q <> %q = %d (%s)\n",
					occurs[fromcat].text,
					occur.text,
					d, desc)
			}
		}
	} else {
		// this ID was not loaded from the catalog, so we don't have any
		// sense of an "original" vs a "duplicate"; so our only mission
		// is to break them up into unique ids
		for _, occur := range occurs[1:] {
			if occur.reassigner != nil {
				proj.fixes = append(proj.fixes, occur.reassigner)
			}
		}
	}
}

func pathFilter(fi os.FileInfo) bool {
	// skip temporary files
	if !fi.Mode().IsRegular() {
		return false
	}
	if strings.HasPrefix(fi.Name(), ".#") {
		return false
	}
	// skip everything except go files
	if !strings.HasSuffix(fi.Name(), ".go") {
		return false
	}
	return true
}

var matchers = make(map[string]*regexp.Regexp)

func globmatcher(g string) *regexp.Regexp {
	if re, ok := matchers[g]; ok {
		return re
	}
	g0 := g
	g = strings.ReplaceAll(g, "**", "{ANY}")
	g = strings.ReplaceAll(g, "*", `[^/]*`)
	g = strings.ReplaceAll(g, ".", `{DOT}`)
	g = strings.ReplaceAll(g, "{DOT}", `[.]`)
	g = strings.ReplaceAll(g, "{ANY}", `.*`)
	re := regexp.MustCompile("^" + g + "$")
	matchers[g0] = re
	return re
}
