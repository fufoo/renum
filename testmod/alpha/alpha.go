package alpha

import (
	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/core/errlib"
)

var log = logging.New("alpha")

func EverythingIsFine() {
	log.Tracef(ctx, "here is a trace")
	log.Debugf(ctx, "AAA-1001 here is some debug")
	log.Warningf(ctx, "AAA-1002 here is a warning")
	log.Infof(ctx, "AAA-1003 here is some info")
	log.Errorf(ctx, "AAA-1004 here is an error")
	log.Auditf(ctx, "AAA-1005 this is an audit record")
}

func MisplacedMessageID() {
	// this is not allowed because of trace=never
	log.Tracef(ctx, "AAA-1234 here is a trace")
}

func DisallowedMessageID() {
	// this is not allowed because of -BBB-2XXX
	log.Infof(ctx, "BBB-2005 here is a trace") // should be renumbered
}

func Wrapped(err error) error {
	return errlib.Wrap("AAA-1990", err) // this should be fine
}

func UnexpectedFollowText(err error) error {
	return errlib.Wrap("AAA-9992 here is something", err)
}

func New(index int) error {
	return errlib.Newf("AAA-9991 here is something: %d", index)
}

type altapi interface {
	Infof(string)
	Wrap(string)
}
	
func Unrecognized(alt altapi) {
	alt.Infof(ctx, "AAA-2000 whoops")
	alt.Wrap("AAA-2001")
}

func NoFollowText() error {
	return errlib.Newf("AAA-2103")
}

func Duplicate() {
	log.Infof(ctx, "AAA-1003 this is a duplicate message id")
}
